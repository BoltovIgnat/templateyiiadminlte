<?php

use yii\db\Migration;

/**
 * Handles the creation of table `projects`.
 * Has foreign keys to the tables:
 *
 * - `users`
 */
class m171023_110906_create_projects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('projects', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer()->notNull(),
            'cost' => $this->integer(),
            'title' => $this->string(),
        ]);

        // creates index for column `author_id`
        $this->createIndex(
            'idx-projects-author_id',
            'projects',
            'author_id'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-projects-author_id',
            'projects',
            'author_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-projects-author_id',
            'projects'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-projects-author_id',
            'projects'
        );

        $this->dropTable('projects');
    }
}
